#!/usr/bin/env python3

import flask
import json
import my_types as types
import util

app = flask.Flask(__name__)
calibrations = None
book_text = None
book_lookup = None
audiobook = None
book_name = None
default_action = 'display_calibrations'


@app.route('/api/text_to_audio')
def text_to_audio_api_endpoint():
    action = default_action

    if 'action' in flask.request.args:
        action = flask.request.args['action']
    if action == 'display_calibrations':
        return {'calibrations': [x.to_dict() for x in calibrations]}
    elif action == 'find_text':
        if 'text' not in flask.request.args:
            payload = {'error': '"text" attribute not found'}
            return flask.Response(json.dumps(payload), status=500, mimetype='application/json')
        else:
            text = flask.request.args['text']
            print(f'lookup: "{text}"')
            response = []
            lookup_locations = book_lookup.lookup(util.simplify(text))
            # TODO: Memoize CalibrationFinder
            calibration_finder = types.CalibrationFinder(calibrations, book_text, audiobook)
            for lookup_location in lookup_locations:
                finder_output = calibration_finder.find(lookup_location)
                print(f'Calibration finder search for "{lookup_location}": {finder_output} ({finder_output.__class__})')
                response.append({'lookup_location': lookup_location, 'track_position': finder_output.to_dict()})

            return {'response': response}
    else:
        payload = {'error': f'unknown action: {action}'}
        return flask.Response(json.dumps(payload), status=500, mimetype='application/json')


@app.route('/js/text_to_audio.js')
def text_to_audio_js_endpoint():
    return flask.render_template('text_to_audio.js', book_name=book_name)


@app.route('/js/display_calibrations.js')
def display_calibrations_js_endpoint():
    return flask.render_template('display_calibrations.js', book_name=book_name)


@app.route('/')
def root_endpoint():
    return flask.render_template('index.html')


def run(my_calibrations, my_book_text, my_book_lookup, my_audiobook, my_book_name):
    global calibrations, book_text, book_lookup, audiobook, book_name
    calibrations = my_calibrations
    book_text = my_book_text
    book_lookup = my_book_lookup
    audiobook = my_audiobook
    book_name = my_book_name
    app.run()
