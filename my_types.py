#!/usr/bin/env python3

import constants
import copy
from mutagen.mp3 import MP3
import os
import subprocess
import util

# initial input
# {"text_filename": "/Users/sean/murmurmatch/generated/crime_and_punishment.txt", "audiobook_directory": "/Users/sean/Downloads/torrents-complete/Fyodor Dostoevsky - Crime and Punishment [V6] Unabridged"}


class WriteAudiobookFilenames:
    name = 'Write audiobook filenames'
    dynamic = False

    def __init__(self, payload):
        if 'audiobook_files' in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'this step has already been run'
            return
        if 'audiobook_directory' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'audiobook_directory key not present in JSON'
            return
        audiobook_directory = payload['audiobook_directory']
        if not os.path.isdir(audiobook_directory):
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'Audiobook directory does not appear to exist'
            return

        payload = copy.deepcopy(payload)
        all_file_paths = []
        for root, subdirs, files in os.walk(audiobook_directory):
            for file in files:
                if any((file.endswith(x) for x in ('.mp3',))):
                    full_path = os.path.join(root, file)
                    if full_path.startswith(audiobook_directory):
                        full_path = full_path[len(audiobook_directory):]
                        if full_path.startswith(os.path.sep):
                            full_path = full_path[len(os.path.sep):]
                            all_file_paths.append(full_path)
                        else:
                            print('THIS SHOULD NOT HAPPEN!')
                    else:
                        print('THIS SHOULD NOT HAPPEN!')
        all_file_paths.sort()
        all_file_path_objects = [{'filename': x} for x in all_file_paths]
        print(f'Got {len(all_file_paths)} files in audiobook')
        payload['audiobook_files'] = all_file_path_objects

        self.step_can_be_run = True
        self.reason_for_inability_to_run_or_result = payload


class WriteAudiobookFileDurations:
    name = 'Write audiobook file durations'
    dynamic = False

    def __init__(self, payload):
        if 'audiobook_directory' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'audiobook_directory key not present in JSON'
        if 'audiobook_files' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'audiobook_files key does not exist'
            return
        files = payload['audiobook_files']
        all_durations_present = True

        payload = copy.deepcopy(payload)

        for file in files:
            if 'duration' not in file:
                all_durations_present = False
                if 'filename' not in file:
                    self.step_can_be_run = False
                    self.reason_for_inability_to_run_or_result = 'filename not present for one or more'
                    return
                path = os.path.join(payload['audiobook_directory'], file['filename'])
                mp3_file = MP3(path)
                length = mp3_file.info.length
                file['duration'] = length

        if all_durations_present:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'this step has already been run'
            return

        self.step_can_be_run = True
        self.reason_for_inability_to_run_or_result = payload


class WriteBookText:
    name = 'Write book text'
    dynamic = False

    def __init__(self, payload):
        if 'text_content' in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'this step has already been run'
            return
        if 'text_filename' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'text_filename key not present in JSON'
            return
        text_filename = payload['text_filename']

        payload = copy.deepcopy(payload)
        try:
            with open(text_filename, 'r') as f:
                text = util.simplify(f.read())
                payload['text_content'] = ' '.join(text)
        except FileNotFoundError:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'text_filename key specifies non-existent file'

        self.step_can_be_run = True
        self.reason_for_inability_to_run_or_result = payload


class PerformCalibration:
    name = 'Perform calibration'
    dynamic = True

    def __init__(self, payload):
        self.error = None
        if 'audiobook_files' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'audiobook_files key not present in JSON'
            return
        if 'audiobook_directory' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'audiobook_directory key not present in JSON'
            return
        if 'text_content' not in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'text_content key not present in JSON'
            return
        if 'calibrations' in payload:
            self.step_can_be_run = False
            self.reason_for_inability_to_run_or_result = 'this step has already been run'
            return
        audiobook_directory = payload['audiobook_directory']
        audiobook_files = payload['audiobook_files']
        text_content = payload['text_content']
        for file in audiobook_files:
            if 'filename' not in file:
                self.step_can_be_run = False
                self.reason_for_inability_to_run_or_result = 'filename key not present for one or more audiobook_files'
                return
            if 'duration' not in file:
                self.step_can_be_run = False
                self.reason_for_inability_to_run_or_result = 'duration key not present for one or more audiobook_files'
                return

        self.payload = copy.deepcopy(payload)

        # Save variables needed in run method
        self.audiobook_directory = audiobook_directory
        self.audiobook_files = audiobook_files
        self.text_content = text_content

        # This is indeed runnable
        self.step_can_be_run = True
        self.reason_for_inability_to_run_or_result = None

    def run(self):
        # TODO: Use this somehow:
        calibrator = Calibrator(self.audiobook_directory, self.audiobook_files, self.text_content)
        if calibrator.error is not None:
            self.error = f'Calibrator error: {calibrator.error}'
            return
        self.payload['calibrations'] = calibrator.calibrations
        return self.payload


class Calibration:
    def __init__(self, calibration_info):
        assert 'start' in calibration_info
        assert 'end' in calibration_info
        assert 'translation' in calibration_info
        assert ('confidence' in calibration_info) == ('position' in calibration_info)

        self.start = calibration_info['start']
        self.end = calibration_info['end']
        self.translation = calibration_info['translation']
        self.confidence = calibration_info['confidence'] if 'confidence' in calibration_info else None
        self.position = calibration_info['position'] if 'position' in calibration_info else None

        self.is_found = 'position' in calibration_info

    def __repr__(self):
        return str(self)

    def __str__(self):
        if self.is_found:
            return f'Calibration(start={self.start}, end={self.end}, translation="{self.translation}" found at {self.position} with confidence {self.confidence})'
        else:
            return f'Calibration(start={self.start}, end={self.end}, translation="{self.translation}" NOT FOUND)'

    def to_dict(self):
        d = {
            'start': self.start,
            'end': self.end,
            'translation': self.translation,
        }
        if self.is_found:
            d = d | {'confidence': self.confidence, 'position': self.position}

        return d


class Calibrator:
    def __init__(self, audiobook_directory, audiobook_files, text_content):
        self.error = None
        samples = []
        audiobook = Audiobook(audiobook_directory, audiobook_files)
        if audiobook.error is not None:
            self.error = f'Audiobook error: {audiobook.error}'
            return
        duration = Duration(audiobook.duration)
        # NOTE: audiobook_files :: [{'filename': string relative path, 'duration': float seconds}]
        # NOTE: text_content :: space-separated string
        # print(f'audiobook_files: {audiobook_files} ({audiobook_files.__class__})')
        # print(f'text_content: {text_content} ({text_content.__class__})')
        print('Total length:', duration)
        ending_offset = 600

        intervals = util.sample_timepoints(0 + ending_offset, audiobook.duration - ending_offset, constants.sample_count(), 10)
        for start, end in intervals:
            audiobook_position_start = audiobook.translate_absolute_position(start)
            print('Start:', audiobook_position_start)
            audiobook_position_end = audiobook.translate_absolute_position(end)
            if audiobook_position_start.track == audiobook_position_end.track:
                print('End:', audiobook_position_end)
            else:
                print('End (before):', audiobook_position_end)
                audiobook_position_end = TrackPosition(audiobook_position_start.track, audiobook_position_start.track.duration)
                print('End (after):', audiobook_position_end)

            value = input('Play? ') if not constants.auto_confirm_yes() else 'yes'
            if value.lower() in ('yes', 'y'):
                print('Understood "yes"')
                # play audio
                translation = audiobook_position_start.track.play(audiobook_position_start.offset, audiobook_position_end.offset)
                # position has track, offset
                # track has duration
                sample = Sample(audiobook_position_start, audiobook_position_end, translation)
                samples.append(sample)
            else:
                print('Understood "no"')
        self.samples = samples

        book = BookText(text_content)
        book_lookup = BookLookup(book)
        calibrations = []
        for sample in samples:
            position_return = util.match_sample(book, book_lookup, sample)
            translation_string = ' '.join(sample.translation)
            base_calibration = {'start': util.translate_to_absolute_position(sample.start, audiobook), 'end': util.translate_to_absolute_position(sample.end, audiobook), 'translation': translation_string}
            if position_return is None:
                print(f'Position of sample "{sample}" could not be found')
                calibrations.append(base_calibration)
            else:
                confidence, position = position_return
                print(f'Position of sample "{sample}" is at {position} with confidence {confidence}')
                calibrations.append(base_calibration | {'confidence': confidence, 'position': position})
        self.calibrations = calibrations


class BookLookup:
    def __init__(self, book):
        # book is instance of BookText; attribute text has simplified text
        self.book = book
        self.gramness = 3
        self.generate_n_grams()

    def generate_n_grams(self):
        book_length = len(self.book.text)
        ngrams_reverse = {i: tuple(self.book.text[i: i + self.gramness]) for i in range(book_length - self.gramness + 1)}
        self.ngrams = util.reverse_dict(ngrams_reverse)

    def lookup(self, translation):
        first_n_words = tuple(translation[:self.gramness])
        if first_n_words not in self.ngrams:
            print('n-gram not found')
            return set()
        else:
            print(f'n-gram found! :: {self.ngrams[first_n_words]}')
            return self.ngrams[first_n_words]


def create_pairs(calibrations):
    pairs = []
    pairs.append(('start', calibrations[0]))
    for first_value_index in range(len(calibrations) - 1):
        pairs.append((calibrations[first_value_index], calibrations[first_value_index + 1]))
    pairs.append((calibrations[-1], 'end'))
    return pairs


def metric_function(metric_function, start_value, end_value):
    def run_metric(value):
        if value == 'start':
            return start_value
        if value == 'end':
            return end_value
        return metric_function(value)

    return run_metric


class CalibrationFinder:
    def __init__(self, calibrations, book_text, audiobook):
        self.calibrations = calibrations
        self.book_text = book_text
        self.audiobook = audiobook

    def find(self, position):
        relevant_calibrations = [x for x in self.calibrations if x.is_found]

        # (('start', calibration_0), (calibration_0, calibration_1), (calibration_1, calibration_2), (calibration_2, calibration_3), (calibration_3, calibration_4), (calibration_4, 'end')) - possible intervals in question
        # look up calibration for each
        pairs = create_pairs(relevant_calibrations)

        get_position = metric_function(lambda x: x.position, 0, len(self.book_text.text))

        for start_calibration, end_calibration in pairs:
            # look up position for each
            start_position, end_position = get_position(start_calibration), get_position(end_calibration)
            if position >= start_position and position < end_position:
                # find out how far between the two in terms of percentage that the input position is from
                difference = end_position - start_position
                percentage = (position - start_position) / difference
                print(f'{position} is between {start_position} and {end_position}, {100 * percentage:.2f}%')
                start_audiobook_position_start = start_calibration.start
                start_audiobook_position_end = start_calibration.end
                end_audiobook_position_start = end_calibration.start
                end_audiobook_position_end = end_calibration.end
                start_audiobook_position = (start_audiobook_position_start + start_audiobook_position_end) / 2
                end_audiobook_position = (end_audiobook_position_start + end_audiobook_position_end) / 2
                print(f'Working out the position that is {100 * percentage:.2f}% along between {start_audiobook_position} and {end_audiobook_position}')

                audiobook_interval_length = end_audiobook_position - start_audiobook_position
                audiobook_interval_percentage_point = audiobook_interval_length * percentage
                new_absolute_position = start_audiobook_position + audiobook_interval_percentage_point
                print('new absolute position:', new_absolute_position)
                return self.audiobook.translate_absolute_position(new_absolute_position)


class BookText:
    def __init__(self, content):
        self.text = util.simplify(content)


class Sample:
    def __init__(self, start_position, end_position, translation):
        self.start = start_position
        self.end = end_position
        self.translation = util.simplify(translation)


class TrackPosition:
    def __init__(self, track, offset):
        self.track = track
        self.offset = offset

    def __repr__(self):
        return str(self)

    def __str__(self):
        return f'{self.track} @ {self.offset}'

    def to_dict(self):
        return {'track': self.track.to_dict(), 'offset': self.offset.to_dict()}


class Track:
    def __init__(self, filename, duration):
        self.filename = filename
        self.duration = duration

    def play(self, start, end):
        with subprocess.Popen(['play', self.filename, 'trim', str(start.total_seconds), f'={end.total_seconds}'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, stdin=subprocess.DEVNULL) as proc:
            translation = input('Enter translation: ')
        return translation

    def __repr__(self):
        return str(self)

    def __str__(self):
        return f'{self.filename} ({self.duration})'

    def to_dict(self):
        return {'filename': self.filename, 'duration': self.duration.to_dict()}


class Audiobook:
    def __init__(self, directory, files):
        tracks = []
        self.error = None
        for file in files:
            path = os.path.join(directory, file['filename'])
            if not os.path.isfile(path):
                self.error = 'some files do not exist'
                return
            track = Track(path, Duration(file['duration']))
            tracks.append(track)

        self.tracks = tuple(tracks)
        self.set_duration()

    def set_duration(self):
        total = 0
        for track in self.tracks:
            total += track.duration.total_seconds
        self.duration = total

    def translate_absolute_position(self, position):
        # determine correct track
        current_file_index = 0
        while position > self.tracks[current_file_index].duration.total_seconds:
            current_file_index += 1
            position -= self.tracks[current_file_index].duration.total_seconds

        track = self.tracks[current_file_index]
        offset = position

        track_position = TrackPosition(track, Duration(offset))
        return track_position


class Duration:
    def __init__(self, seconds):
        self.total_seconds = seconds
        self.parse()

    def parse(self):
        self.hours = int(self.total_seconds // 3600)
        remainder = self.total_seconds % 3600
        self.minutes = int(remainder // 60)
        self.seconds = remainder % 60

    def to_dict(self):
        return {'total_seconds': self.total_seconds, 'formatted': str(self)}

    def __str__(self):
        if self.hours == 0:
            if self.minutes == 0:
                # return f'{self.seconds}s'
                return f'{self.seconds:.2f} seconds'
            else:
                # return f'{self.minutes}m{self.seconds}s'
                return f'{self.minutes} minutes, {self.seconds:.2f} seconds'
        else:
            # return f'{self.hours}h{self.minutes}m{self.seconds}s'
            return f'{self.hours} hours, {self.minutes} minutes, {self.seconds:.2f} seconds'

    def __repr__(self):
        return str(self)
