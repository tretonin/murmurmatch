function formatItem(item) {
  console.log(`Format Item input: ${JSON.stringify(item)}`)

  const percentageComplete = (item.track_position.offset.total_seconds / item.track_position.track.duration.total_seconds) * 100;
  const relativeSpanText = `Out of ${item.track_position.track.duration.formatted} total`;
  return {
    lookup_location: {text: item.lookup_location, mouseover: null},
    track_position_offset_abs: {text: item.track_position.offset.formatted, mouseover: null},
    track_position_offset_rel: {text: `${percentageComplete.toFixed(1)}%`, mouseover: relativeSpanText},
    filename: {text: item.track_position.track.filename, mouseover: null},
    add_command: {text: `murmurmatch addCalibrationForPosition {{ book_name }} ABSOLUTE_TOTAL_SECONDS_MARK 10`, mouseover: null}
  };
}
function setTextField() {
  let textField = document.getElementById("expression");
  console.log(`test! "${textField.value}"`);
  var xhr = new XMLHttpRequest();
  // Assume each formatted response has same keys.
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      let table = document.getElementById("results");
      let firstItem = true;
      let response = JSON.parse(xhr.responseText).response;
      let keys = null;
      for (let item of response) {
        let formattedItem = formatItem(item);
        if (firstItem) {
          let thead = table.createTHead();
          let headerRow = thead.insertRow();
          // keys = Object.keys(formattedItem);
          keys = ["lookup_location", "track_position_offset_abs", "track_position_offset_rel", "filename", "add_command"]
          console.log(`Object: ${JSON.stringify(keys)} (${keys.constructor.name})`);
          for (const key of keys) {
            let th = document.createElement("th");
            let text = document.createTextNode(key);
            th.appendChild(text);
            headerRow.appendChild(th);
          }
          firstItem = false;
        }
        let row = table.insertRow();
        for (const key of keys) {
          let cell = row.insertCell();
          let cellContents = document.createTextNode(formattedItem[key].text);
          if (formattedItem[key].mouseover !== null) {
            const span = document.createElement("span");
            span.setAttribute("title", formattedItem[key].mouseover);
            span.appendChild(cellContents);
            cellContents = span;
          }
          cell.appendChild(cellContents);
        }
      }
    }
  }
  // TODO: is this an issue below about not escaping this? There will be spaces, like, always
  xhr.open('GET', `/api/text_to_audio?action=find_text&text=${textField.value}`)
  xhr.send()
}
