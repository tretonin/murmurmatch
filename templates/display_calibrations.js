function formatItem(item) {
  return {
    confidence: {text: item.confidence, mouseover: null},
    start: {text: `${item.start.toFixed(2)} sec`, mouseover: null},
    end: {text: `${item.end.toFixed(2)} sec`, mouseover: null},
    position: {text: item.position, mouseover: null},
    translation: {text: item.translation, mouseover: null},
    edit_command: {text: `murmurmatch editCalibrationByPosition {{ book_name }} ${item.position}`, mouseover: null}
  }
}

function domReady () {
  getCalibrations();
}

// Mozilla, Opera, Webkit
if ( document.addEventListener ) {
  document.addEventListener( "DOMContentLoaded", function(){
    document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
    domReady();
  }, false );

// If IE event model is used
} else if ( document.attachEvent ) {
  // ensure firing before onload
  document.attachEvent("onreadystatechange", function(){
    if ( document.readyState === "complete" ) {
      document.detachEvent( "onreadystatechange", arguments.callee );
      domReady();
    }
  });
}

function getCalibrations() {
  var xhr = new XMLHttpRequest();
  // Assume each formatted response has same keys.
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      let table = document.getElementById("calibrations");
      let firstItem = true;
      let response = JSON.parse(xhr.responseText).calibrations;
      let keys = null;
      for (let item of response) {
        let formattedItem = formatItem(item);
        console.log(`Formatted item: --${JSON.stringify(formattedItem)}--`)
        if (firstItem) {
          let thead = table.createTHead();
          let headerRow = thead.insertRow();
          // keys = Object.keys(formattedItem);
          keys = ["position", "start", "end", "confidence", "translation", "edit_command"]
          console.log(`Object: ${JSON.stringify(keys)} (${keys.constructor.name})`);
          for (const key of keys) {
            let th = document.createElement("th");
            let text = document.createTextNode(key);
            th.appendChild(text);
            headerRow.appendChild(th);
          }
          firstItem = false;
        }
        let row = table.insertRow();
        for (const key of keys) {
          let cell = row.insertCell();
          let cellContents = document.createTextNode(formattedItem[key].text);
          if (formattedItem[key].mouseover !== null) {
            const span = document.createElement("span");
            span.setAttribute("title", formattedItem[key].mouseover);
            span.appendChild(cellContents);
            cellContents = span;
          }
          cell.appendChild(cellContents);
        }
      }
    }
  }
  xhr.open('GET', "/api/text_to_audio")
  xhr.send()
}
