#!/usr/bin/env python3

import os


apply_strikethrough_to_completed_steps = True
apply_coloring_to_completed_steps = True
default_sample_count = 10
default_auto_confirm_yes = False


def auto_confirm_yes():
    auto_confirm_yes = default_auto_confirm_yes
    if 'MURMURMATCH_AUTO_CONFIRM_YES' in os.environ:
        auto_confirm_yes_string = os.environ['MURMURMATCH_AUTO_CONFIRM_YES']
        try:
            auto_confirm_yes = int(auto_confirm_yes_string) == 1
            print('Overrode default sample count with environment variable value:', auto_confirm_yes)
        except ValueError:
            print(f'Can not parse environment variable sample count! "{auto_confirm_yes_string}"')
    else:
        print('Using default auto_confirm_yes:', auto_confirm_yes)
    return auto_confirm_yes


def sample_count():
    sample_count = default_sample_count
    if 'MURMURMATCH_SAMPLE_COUNT' in os.environ:
        sample_count_string = os.environ['MURMURMATCH_SAMPLE_COUNT']
        try:
            sample_count = int(sample_count_string)
            print('Overrode default sample count with environment variable value:', sample_count)
        except ValueError:
            print(f'Can not parse environment variable sample count! "{sample_count_string}"')
    else:
        print('Using default sample count:', sample_count)
    return sample_count
