#!/usr/bin/env python3

import git
import json
import os
from colorama import Fore, Style


def murmurmatch_dir(ensure_exists=False):
    # return MURMURMATCH_CONFIG_DIR environment variable if defined, ~/.config/murmurmatch if not
    directory = os.path.expanduser('~/.config/murmurmatch')
    if 'MURMURMATCH_CONFIG_DIR' in os.environ:
        directory = os.environ['MURMURMATCH_CONFIG_DIR']
    if ensure_exists:
        os.makedirs(directory, exist_ok=True)
    return directory


def check_match(book, position, translation):
    words_at_position = book.text[position: position + len(translation)]
    for (i, (translation_word, book_word)) in enumerate(zip(translation, words_at_position)):
        if translation_word != book_word:
            # all words before this were correct
            return i / len(translation)
    return 1.0


def sample_timepoints(start_time, end_time, sample_count, sample_length):
    # TODO: Freak out if the samples are two close and will muddle each other
    total_interval = end_time - start_time
    interval = total_interval / sample_count
    intervals = [(start_time + interval * x, start_time + interval * x + sample_length) for x in range(sample_count)]
    return intervals


def reverse_dict(d):
    reversed = {}
    for k, v in d.items():
        if v not in reversed:
            reversed[v] = set()
        reversed[v].add(k)
    return reversed


def red(text):
    return f'{Fore.RED}{text}{Style.RESET_ALL}'


def strikethrough(text):
    return '\u0336' + '\u0336'.join(text)


# TODO: Pre-cache the duration up to the start of each track...

def translate_to_absolute_position(track_position, audiobook):
    position = 0.0

    for track in audiobook.tracks:
        if track == track_position.track:
            position += track_position.offset.total_seconds
            return position

        # before correct track - add entire contents
        position += track.duration.total_seconds


def read_payload(book_name):
    config_dir = murmurmatch_dir(ensure_exists=True)
    was_initialized = git.init_repo(config_dir)
    payload = None
    if was_initialized:
        print('Repository initialized.')
    filename = f'{book_name}.json'
    path = os.path.join(config_dir, filename)
    try:
        with open(path, 'r') as f:
            payload = f.read()
    except FileNotFoundError:
        return None, f'File {path} not found'

    try:
        payload = json.loads(payload)
        return payload, None
    except json.decoder.JSONDecodeError:
        return None, f'File {path} contains invalid JSON'


def match_sample(book, book_lookup, sample):
    # book is instance of BookText; attribute text has simplified text
    # book_lookup is instance of BookLookup
    # sample is instance of Sample; has start & end TrackPositions (has track (filename & duration) & offset) and simplified string translation
    positions = book_lookup.lookup(sample.translation)
    confidence_threshold = 0
    tuples = []
    for position in positions:
        confidence = check_match(book, position, sample.translation)
        t = (confidence, position)
        if confidence >= confidence_threshold:
            tuples.append(t)
    if len(tuples) == 0:
        return
    else:
        print(f'Match ambiguous, there are {len(tuples)} matches')
        return tuples[-1]


def simplify(text):
    text = text.lower()

    # turn dashes into spaces
    text = text.replace('-', ' ')

    # remove newlines
    text = ' '.join(text.splitlines())

    # remove everything that isn't whitespace, or letters, or numbers, to start.
    text = filter(lambda char: char in ' abcdefghijklmnopqrstuvwxyz0123456789', text)
    text = ''.join(text)
    return text.split()
