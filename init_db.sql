create table books (
       id integer primary key,
       created_at datetime not null,
       title string not null,
       path string not null
);
