#!/usr/bin/env python3

import constants
import contextlib
import copy
import git
import json
import logging
import os
import re
import sqlite3
import sys
import time
import my_types as types
import util
import web


BOOK_NAME_REGEX = re.compile(r'^[0-9A-Za-z_-]+$')


@contextlib.contextmanager
def open_db():
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        try:
            cur.execute('select count(*) from books')
            logging.debug('There was NOT an operational error')
        except sqlite3.OperationalError:
            logging.debug('There was an operational error')
            with open('init_db.sql', 'r') as f:
                cur.executescript(f.read())
        yield conn


def lookup_title(text_filename):
    sql = 'select title from books where path = ?'
    with open_db() as conn:
        cur = conn.cursor()
        returned = cur.execute(sql, (text_filename,))
        title = None
        for row in returned:
            if title is None:
                pass
            title = row[0]
            print(f'Got title: {title}')
            return title


def insert_book(title, path):
    sql = 'insert into books (created_at, title, path) values (?, ?, ?)'
    with open_db() as conn:
        cur = conn.cursor()
        returned = cur.execute(sql, (time.time(), title, path))
        print(f'Returned: {returned} ({returned.__class__})')
    # with sqlite3.connect('database.db') as conn:
    #     cur = conn.cursor()


def print_usage():
    print('''
    Usage: murmurmatch init              - initialize a new book
           murmurmatch step BOOKNAME     - perform the next step processing BOOKNAME
    ''')


@contextlib.contextmanager
def open_json_file_for_book_for_writing(config_dir, exclusive=True, book_name=None):
    # Collect name
    # Re-collect name if name is out of character space or file is already found
    #    in config directory (could conceivably add a force, but could also just
    #    tell the user to go delete that file if they want the namespace back.)
    # Yield (file_handle, file_path) tuple
    mode = 'x' if exclusive else 'w'
    while True:
        if book_name is None:
            book_name = input('Book name? ')
            m = BOOK_NAME_REGEX.match(book_name)
            if m is None:
                print('Please keep it to allowed characters')
                continue
        path = os.path.join(config_dir, f'{book_name}.json')
        try:
            with open(path, mode) as f:
                yield (f, path)
                break
        except FileExistsError:
            print(f'File exists already ({path}); please enter another book name')


def get_text_filename():
    # Ask for filename, if it doesn't exist, prompt to confirm
    while True:
        text_filename = input('Text filename? ')
        if os.path.isfile(text_filename):
            break
        if os.path.exists(text_filename):
            print('File exists, but is not regular file. Confirm that this is the path you want:', text_filename)
        else:
            print('File not found. Confirm that this is the path you want:', text_filename)
        yes_no = input('Y/N? ')
        if yes_no.lower() in ('yes', 'y'):
            print('Understood "yes"')
            break
        else:
            print('Understood "no"')
    return text_filename


def get_audiobook_directory():
    # Ask for audiobook directory. If it does not exist, prompt to confirm.
    while True:
        audiobook_directory = input('Audiobook directory? ')
        if os.path.isdir(audiobook_directory):
            break
        if os.path.exists(audiobook_directory):
            print('File exists, but is not directory. Confirm that this is the path you want:', audiobook_directory)
        else:
            print('File not found. Confirm that this is the path you want:', audiobook_directory)
        yes_no = input('Y/N? ')
        if yes_no.lower() in ('yes', 'y'):
            print('Understood "yes"')
            break
        else:
            print('Understood "no"')
    return audiobook_directory


def repo_rollback():
    config_dir = util.murmurmatch_dir(ensure_exists=True)
    git.rollback_last_commit(config_dir)


def init_book():
    config_dir = util.murmurmatch_dir(ensure_exists=True)
    was_initialized = git.init_repo(config_dir)
    if was_initialized:
        print('Repository initialized.')
    with open_json_file_for_book_for_writing(config_dir, exclusive=True) as (f, file_path):
        text_filename = get_text_filename()
        audiobook_directory = get_audiobook_directory()
        f.write(json.dumps({
            'text_filename': text_filename,
            'audiobook_directory': audiobook_directory
        }))
        print('Wrote record to:', file_path)
    git.commit_new_file(file_path)
    print('Added to repository.')


def serve_book(book_name):
    payload, payload_error = util.read_payload(book_name)
    if payload_error is not None:
        print(payload_error)
        return

    # TODO: Do stuff with payload
    if 'calibrations' not in payload:
        print('Calibrations must be configured before the reader interface can be served.')
        return

    if 'text_content' not in payload:
        print('text_content not present!')
        return
    if 'audiobook_files' not in payload:
        print('audiobook_files not present!')
        return
    if 'audiobook_directory' not in payload:
        print('audiobook_directory not present!')
        return
    audiobook = types.Audiobook(payload['audiobook_directory'], payload['audiobook_files'])
    if audiobook.error is not None:
        print('audiobook error:', audiobook.error)
        return

    calibrations = payload['calibrations']
    text_content = payload['text_content']
    book_text = types.BookText(text_content)
    book_lookup = types.BookLookup(book_text)
    calibrations = [types.Calibration(x) for x in calibrations]
    web.run(calibrations, book_text, book_lookup, audiobook, book_name)


def rm_book(book_name):
    config_dir = util.murmurmatch_dir(ensure_exists=True)
    was_initialized = git.init_repo(config_dir)
    if was_initialized:
        print('Repository initialized.')
    filename = f'{book_name}.json'
    path = os.path.join(config_dir, filename)
    try:
        os.remove(path)
        print('Removed:', path)
        git.delete_file(path)
    except FileNotFoundError:
        print(f'File {path} not found')


def step_book(book_name):
    config_dir = util.murmurmatch_dir(ensure_exists=True)
    payload, payload_error = util.read_payload(book_name)
    if payload_error is not None:
        print(payload_error)
        return

    steps = (types.WriteAudiobookFilenames, types.WriteAudiobookFileDurations, types.WriteBookText, types.PerformCalibration)
    runnable_steps = []
    already_run_steps = []
    for step_cls in steps:
        # print('Running step:', step_cls.name)
        step = step_cls(payload)
        step_can_be_run, reason_for_inability_to_run_or_result = step.step_can_be_run, step.reason_for_inability_to_run_or_result
        if step_can_be_run:
            runnable_steps.append((step, reason_for_inability_to_run_or_result, step_cls.dynamic))
        else:
            if reason_for_inability_to_run_or_result == 'this step has already been run':
                already_run_steps.append(step)
            else:
                print(f'Step "{step.name}" cannot be run because: {reason_for_inability_to_run_or_result}')
    for step in already_run_steps:
        step_text = f'0) {step.name}'
        if constants.apply_strikethrough_to_completed_steps:
            step_text = util.strikethrough(step_text)
        if constants.apply_coloring_to_completed_steps:
            step_text = util.red(step_text)
        print(step_text)
    for i, (step, result, is_dynamic) in enumerate(runnable_steps):
        print(f'{i + 1}) {step.name}')
    if len(runnable_steps) == 0:
        print('No runnable steps.')
        return
    while True:
        step_number_string = input('Step to execute? ')
        if step_number_string.lower().strip() in ('q', 'quit'):
            print('Quitting...')
            break
        try:
            step_number = int(step_number_string)
        except ValueError:
            continue
        if step_number < 1 or step_number > len(runnable_steps):
            print('values is not in range; please try again.')
            continue
        step_index = step_number - 1
        step, result, is_dynamic = runnable_steps[step_index]
        if is_dynamic:
            print('Running dynamic step')
            result = step.run()
            if step.error is not None:
                print('Dynamic step threw error, aborting...')
                print('Error:', result.error)
                return
        print('Going to run the following step:')
        print(f'Step: {step.name}')
        print(f'Result: {result}')
        with open_json_file_for_book_for_writing(config_dir, exclusive=False, book_name=book_name) as (f, file_path):
            f.write(json.dumps(result))
        print('Result written.')
        git.commit_file(file_path)
        print('Committed to repo.')
        break


def main(args):
    known_subcommands = ('init', 'rollback', 'step', 'rm', 'serve', 'editCalibrationByPosition', 'addCalibrationForPosition')
    if len(args) < 2 or args[1] not in known_subcommands:
        print_usage()
        return 1

    subcommand = args[1]
    subcommands_that_take_book_logic = {'step': step_book, 'rm': rm_book, 'serve': serve_book}

    if subcommand == 'init':
        init_book()
    elif subcommand == 'rollback':
        repo_rollback()
    elif subcommand in ('step', 'rm', 'serve'):
        if len(args) != 3:
            print_usage()
            return 2
        book_name = args[2]
        func = subcommands_that_take_book_logic[subcommand]
        func(book_name)
    elif subcommand == 'editCalibrationByPosition':
        book_name = args[2]
        if len(args) < 4:
            print('Error!')
            return
        position = args[3]
        try:
            position = int(position)
        except ValueError:
            print('Position is non-numeric')
            return
        payload, payload_error = util.read_payload(book_name)
        if payload_error is not None:
            print(payload_error)
            return
        payload = copy.deepcopy(payload)
        if 'calibrations' not in payload:
            print('Calibrations not found!')
            return
        calibration_indices = [i for (i, calibration) in enumerate(payload['calibrations']) if 'position' in calibration and calibration['position'] == position]
        if len(calibration_indices) > 1:
            print('Non-unique calibration!')
            return
        if len(calibration_indices) == 0:
            print('Calibration not found!')
            return
        calibration_index = calibration_indices[0]
        # get audiobook instance
        audiobook = types.Audiobook(payload['audiobook_directory'], payload['audiobook_files'])
        if audiobook.error is not None:
            print('audiobook error:', audiobook.error)
            return
        start = audiobook.translate_absolute_position(payload['calibrations'][calibration_index]['start'])
        end = audiobook.translate_absolute_position(payload['calibrations'][calibration_index]['end'])
        if start.track != end.track:
            print('Tracks differ!')
            return
        translation = start.track.play(start.offset, end.offset)
        payload['calibrations'][calibration_index]['translation'] = translation
        config_dir = util.murmurmatch_dir(ensure_exists=True)
        with open_json_file_for_book_for_writing(config_dir, exclusive=False, book_name=book_name) as (f, file_path):
            f.write(json.dumps(payload))
        print('Result written.')
        git.commit_file(file_path)
        print('Committed to repo.')

    elif subcommand == 'addCalibrationForPosition':
        book_name = args[2]
        if len(args) < 5:
            print('Error!')
            return
        absolute_seconds = args[3]
        sample_length = args[4]


if __name__ == '__main__':
    # app.run(host='0.0.0.0')
    sys.exit(main(sys.argv))
