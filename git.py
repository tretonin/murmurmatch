#!/usr/bin/env python3

import os
import subprocess


def out(command, output):
    print('Ran command:', command)
    # print(f'Ran command "{command}" with output:')
    # print('-------------------------------------------')
    # print(output)
    # print('-------------------------------------------')


def init_repo(directory):
    args = ['git', 'init']
    command = ' '.join(args)
    output = subprocess.run(args, cwd=directory, stdout=subprocess.DEVNULL)
    # out(command, output)


def commit_new_file(filename):
    commit_file(filename)


def commit_file(filename):
    directory, base_filename = os.path.split(filename)

    args = ['git', 'add', base_filename]
    command = ' '.join(args)
    output = subprocess.run(args, cwd=directory)
    out(command, output)

    args = ['git', 'commit', '-m', 'commit message']
    command = ' '.join(args)
    output = subprocess.run(args, cwd=directory)
    out(command, output)


def rollback_last_commit(directory):
    args = ['git', 'reset', '--hard', 'HEAD~1']
    command = ' '.join(args)
    output = subprocess.run(args, cwd=directory)
    out(command, output)


def delete_file(filename):
    directory, base_filename = os.path.split(filename)

    args = ['git', 'rm', base_filename]
    command = ' '.join(args)
    output = subprocess.run(args, cwd=directory)
    out(command, output)

    args = ['git', 'commit', '-m', 'commit message']
    command = ' '.join(args)
    output = subprocess.run(args, cwd=directory)
    out(command, output)
